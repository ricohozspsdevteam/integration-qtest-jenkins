package test.operations;

import static maths.operations.Addition.addNumbers;
import static maths.operations.Addition.subtractNumbers;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestOperations {
    @Test
    public void testAdd(){
        System.out.println(addNumbers(2, 3));
        Assert.assertTrue(addNumbers(2,3)>0);
    }

    @Test
    public void testSubtract(){
        System.out.println(subtractNumbers(2, 3));
        Assert.assertTrue(subtractNumbers(2,3)>0);
    }
}
